package com.joseluis.barbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.ArrayList;

public class Drinks extends AppCompatActivity implements View.OnClickListener {

    CheckBox opcionVodka, opcionBrandy, opcionTequila, opcionRon, opcionWhisky, opcionCerveza;
    Button botonPreparar;
    ArrayList<CheckBox> listCheckboxes;
    private int tipoBebida = 99;
    private Intent i = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinks);

        inicializarBotones();
        escuchadores();

        listCheckboxes = new ArrayList<>();
        listCheckboxes.add(opcionVodka);
        listCheckboxes.add(opcionBrandy);
        listCheckboxes.add(opcionTequila);
        listCheckboxes.add(opcionRon);
        listCheckboxes.add(opcionWhisky);
        listCheckboxes.add(opcionCerveza);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new Intent().setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.boton_preparar:
                if(tipoBebida != 99){
                    i.setClassName("com.joseluis.barbar", "com.joseluis.barbar.Preparation");
                    i.putExtra("tipoBebida", tipoBebida);
                    startActivity(i);
                }else{
                    Toast.makeText(this, "Selecciona una opción.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.opcion_vodka:
                if (opcionVodka.isChecked()){
                    inhabilitarOpciones(opcionVodka);
                    tipoBebida = 0;
                }
                else
                    habilitarOpciones();
                break;
            case R.id.opcion_brandy:
                if (opcionBrandy.isChecked()){
                    inhabilitarOpciones(opcionBrandy);
                    tipoBebida = 1;
                }
                else
                    habilitarOpciones();
                break;
            case R.id.opcion_tequila:
                if (opcionTequila.isChecked()){
                    inhabilitarOpciones(opcionTequila);
                    tipoBebida = 2;
                }
                else
                    habilitarOpciones();
                break;
            case R.id.opcion_ron:
                if (opcionRon.isChecked()){
                    inhabilitarOpciones(opcionRon);
                    tipoBebida = 3;
                }
                else
                    habilitarOpciones();
                break;
            case R.id.opcion_whisky:
                if (opcionWhisky.isChecked()){
                    inhabilitarOpciones(opcionWhisky);
                    tipoBebida = 4;
                }
                else
                    habilitarOpciones();
                break;
            case R.id.opcion_cerveza:
                if (opcionCerveza.isChecked()){
                    inhabilitarOpciones(opcionCerveza);
                    tipoBebida = 5;
                }
                else
                    habilitarOpciones();
                break;
            default:
                Toast.makeText(this, "Ocurrio un error, opción no válida.", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        clearCheckboxes();
    }

    private void inicializarBotones() {
        opcionVodka = findViewById(R.id.opcion_vodka);
        opcionBrandy = findViewById(R.id.opcion_brandy);
        opcionTequila = findViewById(R.id.opcion_tequila);
        opcionRon = findViewById(R.id.opcion_ron);
        opcionWhisky = findViewById(R.id.opcion_whisky);
        opcionCerveza = findViewById(R.id.opcion_cerveza);

        botonPreparar = findViewById(R.id.boton_preparar);
    }

    private void escuchadores(){
        botonPreparar.setOnClickListener(this);
        opcionVodka.setOnClickListener(this);
        opcionBrandy.setOnClickListener(this);
        opcionTequila.setOnClickListener(this);
        opcionRon.setOnClickListener(this);
        opcionWhisky.setOnClickListener(this);
        opcionCerveza.setOnClickListener(this);
    }

    private void inhabilitarOpciones(CheckBox opcionToIgnore){
        for(int i=0; i<listCheckboxes.size(); i++){
            if(!(listCheckboxes.get(i) == opcionToIgnore))
                listCheckboxes.get(i).setEnabled(false);
        }
    }

    private void habilitarOpciones(){
        for(int i=0; i<listCheckboxes.size(); i++){
            listCheckboxes.get(i).setEnabled(true);
        }
    }

    private void clearCheckboxes(){

        habilitarOpciones();

        opcionVodka.setChecked(false);
        opcionBrandy.setChecked(false);
        opcionTequila.setChecked(false);
        opcionRon.setChecked(false);
        opcionWhisky.setChecked(false);
        opcionCerveza.setChecked(false);

        // Probar solo con finish()
    }
}










