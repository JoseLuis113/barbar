package com.joseluis.barbar;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Preparation extends AppCompatActivity {

    private int opcionElegida = 99;

    private String[] vodkaOpciones;
    private String[] brandyOpciones;
    private String[] tequilaOpciones;
    private String[] ronOpciones;
    private String[] whiskyOpciones;
    private String[] cervezaOpciones;

    TextView title;

    Button botonPreparar;

    String titleDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparation);

        Bundle bundle = getIntent().getExtras();
        opcionElegida = bundle.getInt("tipoBebida");
        title = findViewById(R.id.title_preparation);

        botonPreparar = findViewById(R.id.boton_preparacion_final);

        botonPreparar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarPreparacion().show();
            }
        });

        generarListas();
        cargarOpciones();
    }

    private void generarListas() {

        vodkaOpciones = new String[]{
                "Jugo",
                "Tónica",
                "Sprite"};

        brandyOpciones = new String[]{
                "Refresco de cola",
                "Hielo"};

        tequilaOpciones = new String[]{
                "Refresco de toronja",
                "Paloma",
                "Hielo"};

        ronOpciones = new String[]{
                "Refresco de cola",
                "Refresco de manzana",
                "Hielo"};

        whiskyOpciones = new String[]{
                "Refresco de cola",
                "Refresco de manzana",
                "Hielo"};

        cervezaOpciones = new String[]{
                "Clamato",
                "Michelada",
                "Hielo"};
    }

    private void cargarOpciones(){

        switch (opcionElegida){
            case 0:
                generarCheckboxes(vodkaOpciones);
                titleDialog = "Su bebida es un Vodka";
                title.setText(titleDialog);
                break;
            case 1:
                generarCheckboxes(brandyOpciones);
                titleDialog = "Su bebida es un Brandy";
                title.setText(titleDialog);
                break;
            case 2:
                generarCheckboxes(tequilaOpciones);
                titleDialog = "Su bebida es un Tequila";
                title.setText(titleDialog);
                break;
            case 3:
                generarCheckboxes(ronOpciones);
                titleDialog = "Su bebida es un Ron";
                title.setText(titleDialog);
                break;
            case 4:
                generarCheckboxes(whiskyOpciones);
                titleDialog = "Su bebida es un Whisky";
                title.setText(titleDialog);
                break;
            case 5:
                generarCheckboxes(cervezaOpciones);
                titleDialog = "Su bebida es una Cerveza";
                title.setText(titleDialog);
                break;
            default:
                Toast.makeText(this, "Ocurrio un error, intenta de nuevo", Toast.LENGTH_SHORT).show();
        }

    }

    protected void generarCheckboxes(String[] opciones){

        LinearLayout layoutCheckBoxes = findViewById(R.id.contenedor_hijo_preparacion);

        for (String opcion : opciones) {

            CheckBox opcionCheckbox = new CheckBox(this);
            opcionCheckbox.setText(opcion);

            opcionCheckbox.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
            );
            opcionCheckbox.setTextColor(getResources().getColor(R.color.white));

            layoutCheckBoxes.addView(opcionCheckbox);
        }

    }

    public AlertDialog MostrarPreparacion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Preparation.this);

        builder.setTitle(titleDialog)
                .setMessage("!Su bebida esta preparada¡")
                .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Preparation.this.MostrarPreparacion().dismiss();
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Preparation.this.MostrarPreparacion().dismiss();
                    }
                });
        return builder.create();
    }

}
